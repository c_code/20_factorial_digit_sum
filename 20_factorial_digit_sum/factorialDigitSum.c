#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_DIGITS 5

void print_number(int number[], int num_digits)
{
	int i;

        for(i=0; i < NUM_DIGITS; i++){
		printf("%d", number[i]);
	}
	printf("\n");

}


int add_numbers(int num_one[], int num_two[], int num_digits, int sum[])
{
	int i, temp, ret;
	int *temp_array;
	ret = 0;

	temp_array=malloc(num_digits * sizeof(*temp_array));
	if(temp_array == NULL){
		fprintf(stderr, "could not malloc array in add numbersr\n");
		return -1;
	}

	memcpy(temp_array, num_one, num_digits * sizeof(*temp_array));

	for(i=0;i<num_digits;i++){
		sum[i] = 0;
	}

	temp = 0;

	for(i= num_digits - 1;i >= 0;i--){
		temp_array[i] += temp;
		temp = temp_array[i] + num_two[i];
		sum[i] = temp % 10;
		temp /= 10;
	}

	free(temp_array);
	temp_array = NULL;
	if(temp_array){
		fprintf(stderr, "could not free array in add numbers\n");
		ret = -1;
	}

	return ret;
}

int subtract_numbers(int num_one[], int num_two[], int num_digits, int difference[])
{

	int i, j, temp;

	for(i=0;i<num_digits;i++){
		difference[i] = 0;
	}

	temp = 0;

	for(i= num_digits - 1; i >= 0; i--){
		if(num_one[i] >= num_two[i]){
			difference[i] = num_one[i] - num_two[i];
			printf("%d - %d = %d\n", num_one[i], num_two[i], num_one[i] - num_two[i]);
		}
		else{
			j=i;

			while(!num_one[j] && j > 0){
				j--;
			}

			if(num_one[j]){
				num_one[j]--;
				num_one[i] += 10;
				difference[i] = num_one[i] - num_two[i];
			}
			else{
				fprintf(stderr, "Error: subtracting these numbers will produce a negative result\n");
				return -1;
			}
		}
	}

	return 0;
}

void multiply(int num_one[], int num_two, int num_digits, int product[])
{

	add_numbers(num_one, num_one, num_digits, product);

}

int main()
{

	int num_one[NUM_DIGITS] = {0,1,5,4,5};
	int num_two[NUM_DIGITS] = {0,3,2,4,7};
	int result[NUM_DIGITS] = {0};
	int i,j, ret;

	ret = add_numbers(num_one, num_two, NUM_DIGITS, result);
	if(ret){
		fprintf(stderr, "error adding numbers\n");
		return ret;
	}

	printf("one\n");
	print_number(num_one, NUM_DIGITS);
	printf("two\n");
	print_number(num_two, NUM_DIGITS);
	printf("result\n");
	print_number(result, NUM_DIGITS);

	ret = subtract_numbers(num_two, num_one, NUM_DIGITS, result);
	if(ret){
		fprintf(stderr, "error adding numbers\n");
		return ret;
	}

	printf("one\n");
	print_number(num_one, NUM_DIGITS);
	printf("two\n");
	print_number(num_two, NUM_DIGITS);
	printf("result\n");
	print_number(result, NUM_DIGITS);

	return ret;

}
